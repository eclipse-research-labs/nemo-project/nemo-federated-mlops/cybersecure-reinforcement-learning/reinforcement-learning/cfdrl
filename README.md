# CFDRL

This code merges the reinforcement learning capacities in a centralized federated learning framework build on flower with  attack and defence functionalities

To run the code you use th GUI by running:

python main.py

or run the train script with command lines arguments such as

python train2.py --exp nameofexp --number_clients 5 --exp_conf inputs/exp_optionsDQNacro.json --description description --federated_learning True --port 5833 --scenario attack --attack poison --aggregation krum


The main parameters are:
- number of clients
- aggregation model
- attacks scenarios

With the theredash developped in therlib you can visualize your experiements

<!--- ![alt text](gui.png) -->
<img src="gui.png" width="420">

This code is based on:
- flower
- flwr_attacks develooped by STS available at (https://pypi.org/project/flwr_attacks/)
- Therlib (in the background of nemo) available to the partner of NEMO upon request to victor.gabillon@thalesgroup.com