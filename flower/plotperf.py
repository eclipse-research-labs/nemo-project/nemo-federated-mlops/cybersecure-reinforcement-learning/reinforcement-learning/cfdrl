# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


import pickle

length = 2847
agents = [12, 18]
perf = []

for enum_a, a in enumerate(agents):
    perf.append([])
    for i in range(length):
        perf[enum_a].append([])
        perf[enum_a][i] = pickle.load(open(f'outputs/fed0client_{a}-eval_{i + 1}.txt', 'rb'))
        print(enum_a, i, a, f'outputs/fed0client_{a}-eval_{i + 1}.txt', perf[enum_a][i])

print(perf)

import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
t = np.arange(0.0, length, 1)

fig, ax = plt.subplots()
ax.plot(t, perf[0])
ax.plot(t, perf[1])

ax.set(xlabel='time (s)', ylabel='voltage (mV)',
       title='About as simple as it gets, folks')
ax.grid()

fig.savefig("test.png")
plt.show()
