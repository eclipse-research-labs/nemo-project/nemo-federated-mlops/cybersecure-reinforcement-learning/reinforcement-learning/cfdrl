# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


from typing import Protocol
import numpy as np
import torch


class Actor(Protocol):

    def compute_actions(self, state) -> np.ndarray:
        ...


class BaseActor:

    def __init__(self, device: torch.device,
                 actor_net: torch.nn.Module):
        self.device = device
        self.actor_net = actor_net

    def compute_actions(self, state: np.ndarray) -> np.ndarray:
        model_state = torch.FloatTensor(state).to(self.device).unsqueeze(0)
        action: np.ndarray =  self.actor_net(model_state)
        return action


class ACActor:

    def __init__(self, device: torch.device,
                 actor_net: torch.nn.Module):
        self.device = device
        self.actor_net = actor_net

    def compute_actions(self, state: np.ndarray) -> np.ndarray:
        model_state = torch.FloatTensor(state).to(self.device).unsqueeze(0)
        dist, value = self.actor_net(model_state)
        action: np.ndarray = dist.sample().cpu().numpy().squeeze(0)
        return action



