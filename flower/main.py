# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


"""
Launching the main chipiron
"""

import sys
from typing import Any

from nemo.flower.script_gui import script_gui
from nemo.flower.train2 import train, TrainArgs


def get_script_and_args(
        raw_command_line_arguments: list[str]
) -> TrainArgs:
    """

    Args:
        raw_command_line_arguments: the list of arguments of the scripts given by command line

    Returns:
        A string for the name of script and a dictionary of parameters

    """
    script_type: Any
    # Whether command line arguments are provided or not we ask for more info through a GUI
    if len(raw_command_line_arguments) == 1:  # No args provided
        # use a gui to get user input
        gui_extra_args: TrainArgs
        gui_extra_args = script_gui()
    else:
        raise ValueError('Not expecting command line arguments')

    return gui_extra_args


def main() -> None:
    """
        The main function
    """
    # Getting the command line arguments from the system
    raw_command_line_arguments: list[str] = sys.argv

    # arguments provided to the script from the outside. Here it can be from a gui or command line
    extra_args: TrainArgs

    # extracting the script_name and possibly some input arguments from either the gui or a yaml file or command line
    extra_args = get_script_and_args(raw_command_line_arguments)

    print('extra_args', extra_args, type(extra_args))
    train(args=extra_args)


if __name__ == "__main__":
    main()
