# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


import pathlib
from therlib.logger.agent_logger import AgentLogger as ExpLoggerAgent

from therlib.logger.experiment import create_experiment, stop_experiment
from therlib.memory.replay import ReplayBuffer
from therutils.model.pendulum.deterministic_policy_gradient import LowDPolicyNetwork as PolicyNetwork
from therlib.criterion.dqn import Dqn as DQNCriterion
import numpy as np
from therutils.model.cartpole.q_value import Qvalue

from therlib.noise.action_noise import EpsilonGreedy
import os
from collections import deque, OrderedDict

from therlib.logger.agent_logger import AgentLogger as Agent
from therlib.logger.popagent import PopAgent
import torch

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


class dqnAgent:

    def __init__(self, env, args):
        print('INIT')
        np.random.seed(0)
        self.noise_process_exploration = EpsilonGreedy(args['--epsilon_start'], args['--epsilon_end'],
                                                       args['--epsilon_decay_period'],
                                                       action_space=env.action_space)
        # We create the model
        env.seed(0)

        action_num = env.action_space.n
        state_dim = env.observation_space.shape[0]
        self.model = Qvalue(state_dim, action_num, args['--hidden']).to(DEVICE)
        self.target_model = Qvalue(state_dim, action_num, args['--hidden']).to(DEVICE)

        # We create the optimizer
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=args['--lr'])

        self.step_idx = 0
        self.exp_id = args['--exp_id']

        # We create the replay buffer
        buffer_structure = OrderedDict({
            'state': env.observation_space,
            'action': env.action_space,
            'done': np.zeros(1, dtype=np.uint8),
            'reward': np.zeros(1, dtype=np.float32),
            'next_state': env.observation_space
        })

        # We create the replay buffer
        self.replay_buffer = ReplayBuffer(
            args['--buffer_size'], buffer_structure,
            args['--train_seed']
        )

        # We create the criterion
        self.dqn_criterion = DQNCriterion(
            q_net=self.model,
            q_target_net=self.target_model,
            gamma=args['--gamma'],
            device=DEVICE
        )

        # We prepare the experiment
        self.model_dict = {'model': self.model, 'target_model': self.target_model}
        self.optimizer_dict = {'optimize': self.optimizer}

        ##  self.storage = PopAgent(
        #     args['--exp_id'], args['--client_id'], os.path.join(args['--exp'], 'agent_0'),
        #     {'q_net': self.model},
        #     {'optimizer': self.optimizer}, {}
        #  )

        # self.storage = ExpLoggerAgent(
        #     args['--exp_id'], args['--client_id'], os.path.join(args['--exp'], 'agent_0'),
        #     {'q_net': self.model},
        #          {'optimizer': self.optimizer}
        # )
        self.storage = PopAgent(
            exp_id=args['--exp_id'],
            agent_id=args['--client_id'],
            path=os.path.join(args['--exp'], f'agent_{args["--client_id"]}'),
            keyvalpairs={'o': 2}
        )

    def compute_actions(self, state) -> np.ndarray:
        model_state = torch.FloatTensor(state).to(DEVICE).unsqueeze(0)
        q_values = self.model(model_state).detach().cpu().numpy()
        action = np.array([np.argmax(q_values)]).squeeze(0)

        action = np.array([np.argmax(q_values)])
        action = self.noise_process_exploration.get_action(action=action, t=self.step_idx).squeeze(0)

        return action

    def train(self, env, args):
        """Train the network on the training set."""
        print('train', self.step_idx)

        # We train the model
        episode = 0
        exploration = 1
        train_step_idx = 0
        # for param in self.model.parameters():
        #     print('fgrrrrrrr',param.data)
        #     break
        # print('OOOOOOOOOOOOOOOOOOOOOOOOfrgt', self.model,args['--client_id'])

        while train_step_idx < args['--budget']:
            done = False
            state = env.reset()
            episode = episode + 1
            print('episode', episode)
            episode_reward = 0
            while not done:
                self.model.eval()
                model_state = torch.FloatTensor(state).to(DEVICE).unsqueeze(0)
                # print('rttd model_state',model_state,args['--client_id'],train_step_idx)

                q_values = self.model(model_state).detach().cpu().numpy()
                action = np.array([np.argmax(q_values)])
                action = self.noise_process_exploration.get_action(action=action, t=self.step_idx).squeeze(0)
                # print('action',action,args['--client_id'],train_step_idx)

                next_state, reward, done, _ = env.step(action)
                self.replay_buffer.push(
                    {
                        'state': np.expand_dims(state, 0),
                        'action': np.expand_dims(action, 0),
                        'reward': np.expand_dims(reward, 0),
                        'done': np.expand_dims(done, 0),
                        'next_state': np.expand_dims(next_state, 0),
                    }
                )
                episode_reward += reward
                if self.replay_buffer.can_sample(args['--batch_size']):
                    self.model.train()
                    batch_data = self.replay_buffer.sample(
                        args['--batch_size']
                    )
                    loss = self.dqn_criterion.loss(
                        batch_data['state'], batch_data['action'], batch_data['reward'],
                        batch_data['next_state'], batch_data['done']
                    )
                    # self.storage.performance(train_step_idx, {'loss': loss.item()})
                    # self.storage.performance(train_step_idx, {'score': loss.item()})

                    self.optimizer.zero_grad()
                    loss.backward()
                    self.optimizer.step()
                    if self.step_idx % args['--save_interval'] == 0 and self.step_idx > 1:
                        self.storage.state(epoch=self.step_idx,
                                           models_dict=self.model_dict,
                                           optimizers_dict=self.optimizer_dict)
                        self.storage.performance(self.step_idx, {'loss': loss.item()})
                        self.storage.performance(self.step_idx, {'score': loss.item()})

                        self.storage.write()
                        print('Loss at {}/{}: {:.4}.'.format(
                            self.step_idx, args['--budget'], loss.item()
                        ))
                if self.step_idx % args['--target_net_steps_delay'] == 0:
                    self.target_model.load_state_dict(self.model.state_dict())
                self.step_idx += 1
                train_step_idx += 1
                state = next_state
            print('Episode reward at {}/{}: {}.'.format(
                self.step_idx, args['--budget'], episode_reward
            ))
        env.close()
        self.storage.state(epoch=self.step_idx,
                           models_dict=self.model_dict,
                           optimizers_dict=self.optimizer_dict)
        print('Loss at {}/{}: value1={:.4}.'.format(
            self.step_idx, args['--budget'], loss.item(),
        ))
        self.storage.close()
        stop_experiment(self.exp_id)

        num_examples = args['--budget']
        print('trainEND', self.step_idx)
        self.step_idx = self.step_idx - self.step_idx % args['--budget']
        self.storage.performance(self.step_idx, {'episode_reward': episode_reward})

        return num_examples
