# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


import os
import pathlib
import subprocess

from therlib.logger.experiment import create_experiment
from therutils.parser.criterion.federated import Federated as FederatedParser
from therutils.parser.criterion.federated import TrainArgs, AggregationType


def train(
        args: TrainArgs
) -> None:
    os.environ["CUDA_VISIBLE_DEVICES"] = "2"

    exp_options: dict = {
        'score': {'plot': 'line', 'yscale': 'linear'},
        'episode_reward': {'plot': 'line', 'yscale': 'linear'},
        'loss': {'plot': 'line', 'yscale': 'linear'},
    }

    exp_id: int = create_experiment(
        args.exp, args.description,
        pathlib.Path(__file__).parent.resolve(), exp_options
    )

    serverProcess: subprocess.Popen = subprocess.Popen(
        [
            'python', 'nemo/flower/server.py', '--exp', args.exp,
            '--port', str(args.port),
            '--num_rounds', str(args.num_rounds),
            '--attack', str(args.attacks),
            '--number_clients', str(args.number_clients),
            '--aggregation', str(args.aggregation),
        ]
    )

    numberClients: int = args.number_clients
    client_processes = [None for i in range(numberClients)]

    for i in range(numberClients):
        client_processes[i] = subprocess.Popen(
            ['python', 'nemo/flower/client.py', '--exp', args.exp, '--exp_conf',
             'nemo/flower/inputs/exp_optionsDQNacro.json',
             '--port', str(args.port),
             '--federated_learning', str(args.federated_learning),
             '--client_id', str(i), '--exp_id', str(exp_id),
             '--number_clients', str(args.number_clients),
             '--scenario', str(args.scenario),
             ])

    try:
        serverProcess.wait()
    except:
        serverProcess.kill()
        for i in range(numberClients):
            client_processes[i].kill()


if __name__ == "__main__":
    parser: FederatedParser = FederatedParser()
    args: dict = parser.parse()

    print(args)
    train(args=args)
