# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


import logging
from collections import OrderedDict
from logging import INFO

import bentoml
import flwr as fl
import torch
from flwr.common.logger import log

from data_collection import evaluate
from experiment import init_experiment, Experiment

logging.basicConfig(filename='logname',
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)

logging.info("Running Urban Planning")

logger = logging.getLogger('urbanGUI')


class RLClient(fl.client.NumPyClient):

    def __init__(
            self,
            experiment: Experiment
    ):
        print('init rlclient')
        self.agent = experiment.agent
        self.trainer = experiment.trainer
        self.experiment = experiment
        self.evaluations: int = 0
        self.args = experiment.args
        self.total_number_samples: int = 0
        self.episode = 0

    def get_parameters(self, config):
        self.episode += 1
        print('get_parameters')
        parameters = []
        marker: int = 0

        for model in self.agent.get_model_dict():
            parameters = parameters + [val.cpu().numpy() for _, val in model.state_dict().items()]
            size_model: int = len(model.state_dict().keys())
            params_dict = zip(model.state_dict().keys(), parameters[marker:marker + size_model])
            state_dict = OrderedDict({k: torch.tensor(v) for k, v in params_dict})
            print('sendingt', self.args['--client_id'], state_dict['backbone.0.weight'][0])
            logger.info(f'sendingt {self.episode} {self.args["--client_id"]} {state_dict["backbone.0.weight"][0]}')
            log(INFO, f"rrIIIIIIIInit ServePrinting a custom INFO message at the start of fit() :)")

        return parameters

    def set_parameters(self, parameters):
        print('set parameters', self.args["--federated_learning"])
        if self.args["--federated_learning"]:
            print("--federated_learning")

            marker: int = 0
            for model in self.agent.model_dict.values():
                size_model: int = len(model.state_dict().keys())
                params_dict = zip(model.state_dict().keys(), parameters[marker:marker + size_model])
                state_dict = OrderedDict({k: torch.tensor(v) for k, v in params_dict})
                logger.info(f'receiving {self.episode} {self.args["--client_id"]} {state_dict["backbone.0.weight"][0]}')
                model.load_state_dict(state_dict, strict=True)

                saved_model = bentoml.pytorch.save_model('flower_test',
                                                         model,
                             #                            signatures={"__call__": {
                             #                                "batchable": True,
                             #                                "batch_dim": 0
                             #                            }
                             #                            }
                                                         signatures={  # model signatures for runner inference
                                                             "predict": {
                                                                 "batchable": True,
                                                                 "batch_dim": 0,
                                                             }
                                                         }
                                                         )
                print(f'Models saved: {saved_model} {type(model)}')

            marker += size_model

    def fit(self, parameters, config):
        metrics = {}
        print('fit')
        self.set_parameters(parameters)
        print('client_id', self.experiment.args['--client_id'])
        print('self.trainer ', self.trainer)

        self.trainer.train(self.experiment.env, self.experiment.metric_collection)
        # self.total_number_samples += num_examples
        return self.get_parameters(config={}), 10, metrics

    def evaluate(self, parameters, config):
        self.evaluations += 1
        self.set_parameters(parameters)
        loss = evaluate(actor=self.agent, env=self.experiment.env(), number_evaluations=10)
        accuracy = 0
        return float(loss), 1, {"accuracy": float(accuracy)}


class BenignFlowerClient(RLClient):
    def __init__(self, experiment):
        super().__init__(experiment)

    def fit(self, parameters, config):
        # print("Benign Client Got Selected", flush=True)
        parameters, num_examples, metrics = super().fit(parameters, config)
        metrics["intention"] = "BENIGN"  # just add that the client is benign
        return parameters, num_examples, metrics


class MaliciousFlowerClient(RLClient):
    def __init__(self, experiment):
        super().__init__(experiment)

    def fit(self, parameters, config):
        # print("Malicious Client Got Selected", flush=True)
        parameters, num_examples, metrics = super().fit(parameters, config)
        metrics["intention"] = "MALICIOUS"  # just add that the client is malicious
        return parameters, num_examples, metrics


def create_rlclient(an_experiment):
    print('an_experiment.args[-scenario', an_experiment.args['--scenario'])
    client = RLClient(experiment=an_experiment)
    return client


if __name__ == "__main__":
    # Load model and data
    an_experiment: Experiment = init_experiment()

    an_rlClient: RLClient = create_rlclient(an_experiment)

    # Start Flower client
    fl.client.start_numpy_client(
        server_address=f"127.0.0.1:{an_experiment.args['--port']}",
        client=an_rlClient,
    )
