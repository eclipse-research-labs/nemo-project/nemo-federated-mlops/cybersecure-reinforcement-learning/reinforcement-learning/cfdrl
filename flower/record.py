# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


import sys
import gym
import torch
import numpy as np
from PIL import Image
from tqdm import tqdm
from therenv.space.box import Box
from gym.wrappers.time_limit import TimeLimit
from gym.wrappers.rescale_action import RescaleAction
from gym.spaces.box import Box as GymBox
from therlib.tools.tools import encode_img
from therlib.agents.dqn import DQNAgent
from therutils.model.generic.dqn import make_classic_control


def record_architecture(args):
    """Construct an image of the neural network architecture.
    Args:
        args (dict): Relevant options to construct the image.
    Returns:
        dict: Visualize result of the defined neural network.
    """
    # We create the environment
    env: gym.Env = gym.make(args['--env_name'])

    # We create the actor and critic networks
    model = make_classic_control(
        env.observation_space, env.action_space, args["--hidden"],
        not args["--no_target"]
    )
    # We send the architecture image
    return model.visualize()


def record_model(args, weight_path):
    """Record a sequence of episodes from a neural network.
    Args:
        args (dict): Relevant options to play the episodes.
        weight_path (str): Path of the neural network parameters to load.
    Returns:
        dict: Sequence of episode images buffer.
    """
    # Experiment options
    episode_count = 10
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # We create the environment
    env = gym.make(args['--env_name'])
    if args['--normalize_actions']:
        assert isinstance(env.action_space, (Box, GymBox))
        env = RescaleAction(env, -1, 1)
    if args['--max_steps']:
        env = TimeLimit(env, args['--max_steps'])
    env.seed(args['--train_seed'])

    # We create the actor-critic network
    model = make_classic_control(
        env.observation_space, env.action_space, args["--hidden"],
        not args["--no_target"]
    ).to(device)
    # We create the agent
    agent = DQNAgent(0, model)
    # We load the weights
    agent.load_model(weight_path, device=device)
    model.eval()

    # We view the model
    data = {}
    with torch.no_grad():
        for episode in tqdm(range(episode_count)):
            done = False
            data['episode_{}'.format(episode)] = []
            state = env.reset()
            while not done:
                img = Image.fromarray(env.render(mode='rgb_array'))
                data['episode_{}'.format(episode)].append(encode_img(img))
                action = agent.decide(np.expand_dims(state, 0), use_torch=False)
                state, _, done, _ = env.step(action)
    env.close()
    return data


if __name__ == '__main__':
    if len(sys.argv) < 3:
        raise ValueError(
            'Usage: {} <args> <weight_path>'.format(sys.argv[0])
        )
    import json
    with open(sys.argv[1], "r") as options_f:
        exp_options = json.load(options_f)

    record_architecture(exp_options)
    record_model(exp_options, sys.argv[2])