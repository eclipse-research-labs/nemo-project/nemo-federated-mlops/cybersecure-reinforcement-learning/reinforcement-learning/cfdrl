# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


from therutils.parser.env.gym import Gym as ParserGym
from therutils.parser.experiment import TrainExperiment as TrainExperimentParser
from therutils.parser.model.default import ModelVariousLayers
from therutils.parser.optimizer.adam import Adam
from therutils.parser.criterion.td3 import TD3 as ParserTD3
from therutils.parser.criterion.dqn import DQN as ParserDQN
from therutils.parser.noise import GaussianNoise as ParserGaussian
from therutils.parser.criterion.federated import Client as ParserClient
from therlib.metric_collection.metrics import LossMetric

import gym
import os
from therutils.parser.noise import EpsilonGreedy as EpsilonGreedyParser
from therutils.parser.replay import Replay as ReplayParser

from gym.wrappers.rescale_action import RescaleAction
from gym.wrappers.time_limit import TimeLimit

from therutils.model.generic.dqn import make_classic_control

from dataclasses import dataclass
from typing import Any

from therlib.agents.agent import Agent
from therlib.trainer.trainer import Trainer
from therlib.metric_collection.metric_collection import MetricCollection
import torch

from therlib.agents.dqn import DQNAgent
from therlib.trainer.dqn import DQN as DQNTrainer
from therlib.logger.popagent import PopAgent

from therlib.memory.utils import create_buffer_structure
from therlib.memory.replay import ReplayBuffer, PrioritizedReplayBuffer

from therlib.noise.action_noise import EpsilonGreedy


@dataclass
class Experiment:
    """Experiment """
    env: Any
    agent: Agent
    args: dict
    trainer: Trainer
    metric_collection: MetricCollection


def make_env(args):
    def _init():
        env = gym.make(args['--env_name'])
        if '--normalize_actions' in args and args['--normalize_actions']:
            env = RescaleAction(env, -1, 1)
        if args['--max_steps']:
            env = TimeLimit(env, args['--max_steps'])
        env.seed(args['--train_seed'])
        return env

    return _init


class DQNAgent_modified(DQNAgent):

    def __init__(self, agent_id, model, runnable_env):
        super().__init__(agent_id, model)
        self.model_dict = {'model': self.model}
        self.step_idx = 0

    def get_model_dict(self):
        return self.model_dict.values()


class DQNTrainerFLWrapper:

    def __init__(self, dqn_trainer, runnable_env):
        self.dqn_trainer = dqn_trainer
        self.step_idx = 0
        self.envs = self.dqn_trainer.build_parallel_envs(runnable_env)

        # We create the replay buffer
        buffer_structure = create_buffer_structure(
            self.envs.observation_space, self.envs.action_space,
            self.dqn_trainer._replay_processing,
            add_scenario_id=False
        )
        if self.dqn_trainer.use_per:
            print('yes PrioritizedReplay')
            self.replay = PrioritizedReplayBuffer(
                self.dqn_trainer.buffer_size, buffer_structure, self.dqn_trainer.per_alpha,
                self.dqn_trainer.per_beta, self.dqn_trainer.train_seed
            )
        else:
            print('no PrioritizedReplay')
            self.replay = ReplayBuffer(
                self.dqn_trainer.buffer_size, buffer_structure, self.dqn_trainer.train_seed
            )

        # We create the epsilon_greedy exploration process
        self.exploration_process = EpsilonGreedy(
            self.dqn_trainer.epsilon_start, self.dqn_trainer.epsilon_end, self.dqn_trainer.epsilon_decay,
            self.dqn_trainer.action_space, self.dqn_trainer.train_seed
        )

    def train(self, runnable_env, metric_collection):

        print('self.dqn_trainer.budget', self.dqn_trainer.budget)
        # We train the model
        state = self.envs.reset()
        frame_idx = 0
        while frame_idx < self.dqn_trainer.budget + 1:
            if frame_idx % 100 == 0:
                print(frame_idx)
            # Do one step of training: interact with the environment to fill
            # the rollout, and then perform one dqn update.
            self.step_idx, state = self.dqn_trainer.train_one_step(
                self.step_idx, state, self.envs, self.replay, metric_collection,
                self.exploration_process
            )
            frame_idx += 1
            # End of the training, save the last weights and cleanup everything.

        models_dict, optimizer_dicts = self.dqn_trainer.extract_models_and_optimizers()
        self.dqn_trainer.logger.state(self.dqn_trainer.budget, models_dict, optimizer_dicts)
        self.dqn_trainer.logger.close()


def make_env_agent_and_trainer(
        args: dict,
        exp_id: int,
        agent_id: int,
        device
):
    runnable_env = make_env(args)
    env = runnable_env()
    # We create the DQN model
    model = make_classic_control(
        env.observation_space, env.action_space,
        hidden_dim=args["--hidden"], use_target=not args["--no_target"]
    ).to(device)

    # We create the agent
    agent = DQNAgent_modified(agent_id, model, runnable_env)

    logger = PopAgent(
        exp_id=args['--exp_id'],
        agent_id=args['--client_id'],
        path=os.path.join(args['--exp'], f'agent_{args["--client_id"]}'),
        keyvalpairs={'o': 2}
    )

    # We create the trainer
    trainer_: DQNTrainer = DQNTrainer(
        exp_id, agent_id, agent,
        path=os.path.join(args['--exp'], 'agent_0'),
        train_seed=args["--train_seed"],
        gamma=args['--gamma'],
        batch_size=args['--batch_size'],
        budget=args['--budget'],
        save_interval=args['--save_interval'],
        weights_interval=args["--weight_interval"],
        checkpoint=args['--checkpoint'],
        use_per=args["--use_per"],
        per_alpha=args["--per_alpha"],
        per_beta=args["--per_beta"],
        buffer_size=args['--buffer_size'],
        lr=args['--lr'],
        epsilon_start=args['--epsilon_start'],
        epsilon_end=args['--epsilon_end'],
        epsilon_decay=args['--epsilon_decay_period'],
        use_target=not args["--no_target"],
        double_dqn=args["--double_dqn"],
        target_update_period=args['--target_net_steps_delay'],
        n_step_returns=args["--n_step_returns"],
        num_envs=args["--num_envs"] or 1,
        logger=logger
    )

    trainer = DQNTrainerFLWrapper(
        dqn_trainer=trainer_,
        runnable_env=runnable_env
    )

    return runnable_env, agent, trainer


def make_metric_collection(args):
    # Losses
    loss_keys = [
        'loss'
    ]
    losses = [
        LossMetric(loss_key) for loss_key in loss_keys
    ]
    return MetricCollection(
        num_envs=args["--num_envs"] or 1,
        score_buffer_size=50,
        collect_episode_length=False,
        losses=losses,
        score_metric="episode_reward"
    )


def init_experiment():
    # Parse the options
    parser = TrainExperimentParser(env=ParserGym(),
                                   model=ModelVariousLayers(),
                                   optimizer=Adam(),
                                   criterion=ParserClient(ParserDQN(EpsilonGreedyParser(), ReplayParser())))
    args = parser.parse()
    print('args experiments', args)

    agent_id = args['--client_id']
    exp_id = args['--exp_id']
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    runnable_env, agent, trainer = make_env_agent_and_trainer(args, exp_id, agent_id, device)

    metric_collection = make_metric_collection(args)

    experiment = Experiment(env=runnable_env,
                            agent=agent,
                            args=args,
                            trainer=trainer,
                            metric_collection=metric_collection)
    return experiment
