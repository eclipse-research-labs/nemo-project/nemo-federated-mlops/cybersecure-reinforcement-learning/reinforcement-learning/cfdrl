# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


import multiprocessing
from experiment import Experiment
from typing import List, Tuple

import flwr as fl
from flwr.common import Metrics


from logging import INFO, DEBUG
from flwr.common.logger import log

def weighted_average(metrics: List[Tuple[int, Metrics]]) -> Metrics:
    # Multiply accuracy of each client by number of examples used
    accuracies = [num_examples * m["accuracy"] for num_examples, m in metrics]
    examples = [num_examples for num_examples, _ in metrics]

    # Aggregate and return custom metric (weighted average)
    return {"accuracy": sum(accuracies) / sum(examples)}


class ServerProcess(multiprocessing.Process):
    def __init__(self, experiment: Experiment):
        print(f'IIIIIIIIIIInit Server')
        log(INFO, f"IIIIIIIInit ServePrinting a custom INFO message at the start of fit() :)")
        # Call the Thread class's init function
        multiprocessing.Process.__init__(self, daemon=False)
        self._stop_event = multiprocessing.Event()
        self.experiment = experiment


    # Override the run() function of Thread class
    def run(self):
        print('Started Server Process : ')

        while not self.stopped():
            # Start Flower client
            fl.server.start_server(
                server_address="0.0.0.0:8080",
                config=fl.server.ServerConfig(num_rounds=30),
                strategy=self.strategy,
            )

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()
