# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


from therlib.criterion.util import soft_update
from therlib.logger.agent_logger import AgentLogger as ExpLoggerAgent

from therlib.logger.experiment import create_experiment, stop_experiment
from therlib.memory.replay import ReplayBuffer
from therutils.model.pendulum.deterministic_policy_gradient import LowDPolicyNetwork as PolicyNetwork
from therutils.model.pendulum.deterministic_policy_gradient import LowDActionValueNetworkTD3 as ActionValueNetwork
import numpy as np
from therlib.criterion.td3 import Td3 as TD3Criterion
import os
from therlib.noise.action_noise import Gaussian
from collections import deque, OrderedDict

import torch

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

class TD3Agent:

    def __init__(self, env, args):
        # We create the action noise process for exploration around the behavior policy
        self.noise_process_exploration = Gaussian(env.action_space, args['--g_min_sigma'], args['--g_max_sigma'])

        # We create the value and policy networks as well as their target
        action_dim = env.action_space.shape[0]
        state_dim = env.observation_space.shape[0]
        self.critic_net1, self.critic_net2, self.target_critic_net1, self.target_critic_net2 = [
            ActionValueNetwork(state_dim, action_dim, args['--hidden']).to(DEVICE)
            for _ in range(4)
        ]
        self.actor_net, self.target_actor_net = (
            PolicyNetwork(state_dim, action_dim, args['--hidden']).to(DEVICE),
            PolicyNetwork(state_dim, action_dim, args['--hidden']).to(DEVICE)
        )

        env.seed(0)
        # We create the optimizers
        self.actor_optimizer = torch.optim.Adam(self.actor_net.parameters(), lr=args['--policy_lr'])
        self.critic1_optimizer = torch.optim.Adam(self.critic_net1.parameters(), lr=args['--value_lr'])
        self.critic2_optimizer = torch.optim.Adam(self.critic_net2.parameters(), lr=args['--value_lr'])

        # We initialize the target models to be identical to the other models
        soft_update(self.critic_net1, self.target_critic_net1, soft_tau=1.)
        soft_update(self.critic_net2, self.target_critic_net2, soft_tau=1.)
        soft_update(self.actor_net, self.target_actor_net, soft_tau=1.)

        # We create the replay buffer
        buffer_structure = OrderedDict({
            'state': env.observation_space,
            'action': env.action_space,
            'done': np.zeros(1, dtype=np.uint8),
            'reward': np.zeros(1, dtype=np.float32),
            'next_state': env.observation_space
        })
        self.replay_buffer = ReplayBuffer(
            args['--buffer_size'], buffer_structure,
            args['--train_seed']
        )

        # We create the criterion
        self.td3_criterion = TD3Criterion(
            self.actor_net, self.target_actor_net,
            self.critic_net1, self.critic_net2, self.target_critic_net1, self.target_critic_net2,
            gamma=args['--gamma'], soft_tau=args['--soft_tau'],
            noise_std=args['--g_smooth_sigma'], noise_clip=args['--g_smooth_clip'],
            device=DEVICE
        )

        # We prepare the experiment
        exp_options = {
            'episode_reward_train': {'plot': 'line', 'yscale': 'linear'},
            'episode_reward_test': {'plot': 'line', 'yscale': 'linear'},
            'score': {'plot': 'line', 'yscale': 'linear'},
            'loss': {'plot': 'line', 'yscale': 'log'},
            'actor_loss': {'plot': 'line', 'yscale': 'log'},
            'critic_loss_1': {'plot': 'line', 'yscale': 'log'},
            'critic_loss_2': {'plot': 'line', 'yscale': 'log'},
        }
        agent_id = 0
        description = 'Td3: {} with {} frames for training'.format(
            args['--env_name'], args['--budget']
        )
        exp_id = create_experiment(args['--exp'], description, './', exp_options)
        self.storage = ExpLoggerAgent(
            exp_id, agent_id, os.path.join(args['--exp'], 'agent_0'),
            {'critic_model1': self.critic_net1, 'critic_model2': self.critic_net2, 'actor_model': self.actor_net},
            {'critic1_optimizer': self.critic1_optimizer, 'critic2_optimizer': self.critic2_optimizer,
             'actor_optimizer ': self.actor_optimizer}
        )

        self.reward_buffer = deque(maxlen=100)

        self.model_list = [self.critic_net1, self.critic_net2, self.target_critic_net1,
                           self.target_critic_net2, self.actor_net, self.target_actor_net]

        self.step_idx = 0


    def compute_actions(self, state) -> np.ndarray:
        model_state = torch.FloatTensor(state).to(DEVICE).unsqueeze(0)
        action = self.actor_net(model_state).detach().cpu().numpy()
        return action

    def train(self, env, args):
        """Train the network on the training set."""

        print('OOOOOOOOOOOOOOOOOOOOOOOOfrgt',self.actor_net)

        # We train the networks
        episode_reward_train = 0
        state = env.reset()
        train_step_idx = 0
        while train_step_idx < args['--budget']:
            self.actor_net.eval()

            # Do one step in the environment and save information
            model_state = torch.FloatTensor(state).to(DEVICE).unsqueeze(0)
            action = self.actor_net(model_state).detach().cpu().numpy()
            action = self.noise_process_exploration.get_action(action).squeeze(0)
            next_state, reward, done, _ = env.step(action)
            self.replay_buffer.push(
                {
                    'state': np.expand_dims(state, 0),
                    'action': np.expand_dims(action, 0),
                    'reward': np.expand_dims(reward, 0),
                    'done': np.expand_dims(done, 0),
                    'next_state': np.expand_dims(next_state, 0),
                }
            )
            episode_reward_train += reward
            self.storage.performance(self.step_idx, {'reward': reward})

            # Train/Update the actor and critic based on resampling transitions from the replay buffer
            if self.step_idx % args['--delay_policy_update'] == 0:
                self.actor_net.train()
            self.critic_net1.train()
            self.critic_net2.train()
            if self.replay_buffer.can_sample(args['--batch_size']):
                # Sample from the relay buffer
                batch_data = self.replay_buffer.sample(
                    args['--batch_size']
                )
                # Compute, store and optimize the losses
                critic_loss1, critic_loss2, actor_loss = self.td3_criterion.loss(
                    batch_data['state'], batch_data['action'], batch_data['reward'],
                    batch_data['next_state'], batch_data['done']
                )
                self.storage.performance(self.step_idx, {
                    'critic1_loss': critic_loss1.item(),
                    'critic2_loss': critic_loss2.item(),
                    'actor_loss': actor_loss.item()
                })

                self.critic1_optimizer.zero_grad()
                critic_loss1.backward(retain_graph=True, inputs=list(self.critic_net1.parameters()))
                self.critic2_optimizer.zero_grad()
                critic_loss2.backward(retain_graph=True, inputs=list(self.critic_net2.parameters()))
                if self.step_idx % args['--delay_policy_update'] == 0:
                    self.actor_optimizer.zero_grad()
                    actor_loss.backward(inputs=list(self.actor_net.parameters()))
                self.critic1_optimizer.step()
                self.critic2_optimizer.step()
                if self.step_idx % args['--delay_policy_update'] == 0:
                    self.actor_optimizer.step()
                    soft_update(self.critic_net1, self.target_critic_net1, args['--soft_tau'])
                    soft_update(self.critic_net2, self.target_critic_net2, args['--soft_tau'])
                    soft_update(self.actor_net, self.target_actor_net, args['--soft_tau'])

                # Save and print performance information
                if self.step_idx % args['--save_interval'] == 0 and self.step_idx > 1 and len(
                        self.reward_buffer) > 1:
                    mean_reward = sum(self.reward_buffer) / len(self.reward_buffer)
                    self.storage.performance(self.step_idx, {'score': mean_reward})
                    self.storage.state(self.step_idx)
                    self.storage.write()
                    print('Loss at {}/{}: value1={:.4}, value2={:.4},  policy={:.4}.'.format(
                        self.step_idx, args['--budget'], critic_loss1.item(), critic_loss2.item(), actor_loss.item()
                    ))
                    print('Result at {}/{}: {}.'.format(
                        self.step_idx, args['--budget'], mean_reward
                    ))

            self.step_idx += 1
            train_step_idx +=1
            state = next_state

            if done:  # the episode came to an end.

                print('Training episode reward at {}/{}: {}.'.format(
                    self.step_idx, args['--budget'], episode_reward_train
                ))
                self.storage.performance(self.step_idx, {'episode_reward_train': episode_reward_train})
                episode_reward_train = 0

                # Testing the learned policy on one episode
                self.actor_net.eval()
                total_number_test = 1
                episode_reward_test = 0
                for test_number in range(total_number_test):
                    state_test = env.reset()
                    done_test = False
                    while not done_test:
                        model_state_test = torch.FloatTensor(state_test).to(DEVICE)
                        action_test = self.actor_net(model_state_test).detach().cpu().numpy()
                        next_state_test, reward_test, done_test, info_test = env.step(action_test)
                        episode_reward_test += reward_test
                        state_test = next_state_test

                normalized_episode_reward_test = episode_reward_test / total_number_test
                print('Test Episode reward at {}/{}: {}.'.format(
                    self.step_idx, args['--budget'], normalized_episode_reward_test
                ))

                self.reward_buffer.append(normalized_episode_reward_test)
                self.storage.performance(self.step_idx, {'episode_reward_test': episode_reward_test})

                state = env.reset()

        num_examples = args['--budget']
        return num_examples

