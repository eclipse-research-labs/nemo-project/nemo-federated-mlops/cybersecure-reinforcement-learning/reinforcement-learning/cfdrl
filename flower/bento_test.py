# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


import bentoml
from bentoml.io import JSON
from bentoml.io import NumpyNdarray
import torch
from torch.autograd import Variable
import numpy as np

runner = bentoml.pytorch.get('flower_test:latest').to_runner(embedded=True)
#runner.init_local()
#a=runner.predict.run(Variable(torch.FloatTensor( [0,0,0,0,0,0] )))
#print('rrr',a)
svc = bentoml.Service(name="test_service", runners=[runner])


#
#
# @svc.api(input=NumpyNdarray(), output=NumpyNdarray())
# async def predict(np_array):
#      print('n', np_array)
#      x = Variable(torch.FloatTensor(np_array))
#      batch_ret = await runner.async_run(Variable(torch.FloatTensor( [0,0,0,0,0,0] )))
#      print('rr', batch_ret, type(batch_ret))
#      res = np.ndarray(batch_ret)
#      return batch_ret

#@svc.api(input=NumpyNdarray(), output=NumpyNdarray())
#def predicto(np_array):
#    print('n', np_array)
#    x = Variable(torch.FloatTensor(np_array))##

#    batch_ret = runner.predict.run(Variable(torch.FloatTensor([0, 0, 0, 0, 0, 0])))
#    print('rr', batch_ret, type(batch_ret))
#    res = np.ndarray(batch_ret)
#    return batch_ret


@svc.api(input=NumpyNdarray(), output=NumpyNdarray())
async def predicto(np_array):
    print('n', np_array)
    x = Variable(torch.FloatTensor(np_array))
    batch_ret = await runner.async_run(Variable(torch.FloatTensor(x)))
    print('rr', batch_ret, type(batch_ret))
    res = batch_ret.cpu().numpy()
    return res
