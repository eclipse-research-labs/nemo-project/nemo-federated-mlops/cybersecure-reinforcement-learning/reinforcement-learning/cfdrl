# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


from dataclasses import dataclass
from typing import List
import numpy as np


# @dataclass
# class TransSD:
#     state: list
#     done: bool


@dataclass
class Transition:
    # TODO not that clear we actually follow these type annotations at the moment, like reward is a float i think
    state: np.ndarray
    action: np.ndarray
    reward: np.ndarray
    next_state: np.ndarray
    done: np.ndarray
    info: dict


Trajectory = List[Transition]
Trajectories = List[Trajectory]
