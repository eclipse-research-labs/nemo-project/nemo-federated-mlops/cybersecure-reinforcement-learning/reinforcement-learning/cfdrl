# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


import logging
import pickle
import numpy as np
import dataclasses, json
from common import Actor
from data import Trajectories, Transition, Trajectory  # TransSD

from therenv.core import Env

my_logger = logging.getLogger(__name__)
my_logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(levelname)s  %(message)s')
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
my_logger.addHandler(stream_handler)
my_logger.propagate = False

from therlib.agents.agent import Agent

class EnhancedJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if dataclasses.is_dataclass(o):
            return dataclasses.asdict(o)
        return super().default(o)


def store_trajectories_to_file(trajectories: Trajectories,
                               file_path: str) -> None:
    with open(file_path, 'wb') as file:
        pickle.dump(trajectories, file)


def store_trajectories_to_json(trajectories, file_path):
    with open(file_path, 'w') as file:
        json.dump(trajectories, file, cls=EnhancedJSONEncoder)


def read_trajectories_from_file(file_path: str) -> Trajectories:
    with open(file_path, 'rb') as file:
        trajectories = pickle.load(file)

    return trajectories


def collect_changing_trajectories(actor: Actor,
                                  env: Env,
                                  deployer):
    my_logger.info('starting collect_training_data')
    trajectories: Trajectories = []

    for _ in range(deployer.number_trajectories):
        one_trajectory: Trajectory = play_one_episode_render(env=env, actor=actor, deployer=deployer)
        trajectories.append(one_trajectory)

    my_logger.info(f'{len(trajectories)} trajectories collected')
    return trajectories


def collect_training_data(actor: Actor,
                          env: Env,
                          number_of_trajectories: int) -> Trajectories:
    """


    Args:
        actor:   the actor to play
        env: the environment
        number_trajectories:  number of trajectories to collect after training

    Returns:
        trajectories: the collected trajectories
    """
    my_logger.info('starting collect_training_data')
    trajectories: Trajectories = []

    for _ in range(number_of_trajectories):
        one_trajectory: Trajectory = play_one_episode(env=env, actor=actor)
        trajectories.append(one_trajectory)

    my_logger.info(f'{len(trajectories)} trajectories collected')
    return trajectories


def play_one_episode_render(env: Env,
                            actor: Actor,
                            deployer=None) -> Trajectory:
    my_logger.debug('play one episode')
    done: bool = False
    state: np.ndarray = env.reset()
    the_rollout: Trajectory = []
    next_state: np.ndarray
    time = 0
    env.change(sc.env.CartpoleChangeArgs(length=0.5))
    while not done:
        time += 1

        if time == deployer.change_step:
            env.change(deployer.change)
            my_logger.info(f'Pole length chnaged to {deployer.change} at {time}')

        action: np.ndarray = actor.compute_actions(state)
        next_state, reward, done, info = env.step(action)

        env.render()

        if not action.shape:
            # TODO looks a big hacky, can we make it better and work for any env?
            action = np.expand_dims(action, 0)

        if state.size > 0:  # the stratis env sometimes gives empty states so
            transition: Transition = Transition(
                state=state,
                action=action,
                next_state=next_state,
                reward=reward,
                done=done,
                info=info
            )
        env.render()
        the_rollout.append(transition)

        state = next_state

    return the_rollout


def play_one_episode(env: Env,
                     actor: Agent,
                     ) -> Trajectory:
    my_logger.debug('play one episode')
    done: bool = False
    state: np.ndarray = env.reset()
    the_rollout: Trajectory = []
    next_state: np.ndarray
    time = 0
    while not done:
        time += 1

        action: np.ndarray = actor.decide([state])
        next_state, reward, done, info = env.step(action)

        if not action.shape:
            # TODO looks a big hacky, can we make it better and work for any env?
            action = np.expand_dims(action, 0)

        if state.size > 0:  # the stratis env sometimes gives empty states so
            transition: Transition = Transition(
                state=state,
                action=action,
                next_state=next_state,
                reward=reward,
                done=done,
                info=info
            )

            the_rollout.append(transition)

        state = next_state

    return the_rollout


def evaluate_from_trajectories(trajectories: Trajectories) -> np.ndarray:
    """
    Args:
        trajectories: the Trajectories

    Returns:
        average_return: np.ndarray
    """
    my_logger.info('starting collect_training_data')

    average_return: float = 0.

    trajectory: Trajectory
    for trajectory in trajectories:

        episode_return: float = 0.
        transition: Transition
        for transition in trajectory:
            # UGLY HARDCODE
            if isinstance(transition.reward, float):
                episode_return += transition.reward
            else:  # stratis
                reward = transition.reward.mean(axis=0)
                lambda_ = 2
                episode_return += reward[1]

        average_return += episode_return
    average_return = average_return / len(trajectories)

    return average_return


def evaluate(actor: Agent,
             env: Env,
             number_evaluations: int) -> float:
    """


    Args:
        actor:   the actor to play
        env: the environment
        number_evaluations:  number of trajectories to collect after training

    Returns:
        average_return: float
    """
    my_logger.info('starting collect_training_data')

    average_return: float = 0.

    for _ in range(number_evaluations):
        one_trajectory: list = play_one_episode(env=env, actor=actor)
        episode_return: float = 0.
        transition: Transition
        for transition in one_trajectory:
            episode_return += transition.reward
        average_return += episode_return
    average_return = average_return / float(number_evaluations)

    return average_return


def print_collected_training_data(data: list):
    for idx, trajectory in enumerate(data):
        print('trajectory idx {} of length {}'.format(idx, len(trajectory)))
        for transition in trajectory:
            print('transition', transition)
