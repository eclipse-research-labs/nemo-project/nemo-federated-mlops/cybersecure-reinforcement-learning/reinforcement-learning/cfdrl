# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


from logging import INFO, ERROR
from typing import List, Tuple

import flwr as fl
from flwr.common import Metrics
from flwr.common.logger import log
from flwr_attacks import MinMax, AttackServer, generate_cids

from therutils.parser.criterion.federated import Server as ServerParser


from therutils.parser.criterion.federated import AttacksType, AggregationType

# Define metric aggregation function
def weighted_average(metrics: List[Tuple[int, Metrics]]) -> Metrics:
    # Multiply accuracy of each client by number of examples used
    accuracies = [num_examples * m["accuracy"] for num_examples, m in metrics]
    examples = [num_examples for num_examples, _ in metrics]

    # Aggregate and return custom metric (weighted average)
    return {"accuracy": sum(accuracies) / sum(examples)}


# Define strategy
strategy = fl.server.strategy.FedAvg(evaluate_metrics_aggregation_fn=weighted_average)


def server():
    # Start Flower server

    parser: ServerParser = ServerParser()
    args: dict = parser.parse()
    log(INFO, f"aarrIIIIIIIInit ServePrinting a custom INFO message at the start of fit() :){args}")

    adversary_cids, benign_cids = generate_cids(args['--number_clients'], adversary_fraction=0.4)
    all_cids = adversary_cids + benign_cids

    match args['--attack']:
        case AttacksType.NO_ATTACKS:
            log(INFO, "no attack")
            attack = MinMax(
                adversary_fraction=0,  # 20% of clients are adversaries
                activation_round=5000000000000000000,  # Activate attack at round 5
                adversary_clients=adversary_cids,
                # by default the attack will be able to access only the adversary clients. Use the argument adversary_accessed_cids to add specific access.
            )
        case AttacksType.MIN_MAX:
            log(INFO, "min_max")
            attack = MinMax(
                adversary_fraction=0.2,  # 20% of clients are adversaries
                activation_round=5,  # Activate attack at round 5
                adversary_clients=adversary_cids,
                # by default the attack will be able to access only the adversary clients. Use the argument adversary_accessed_cids to add specific access.
            )
            # <-- pass the metric aggregation function. This function will be called
        # in every federated learning round for evaluation (it aggregates the
        # client-side evaluation metrics in the server)
        case other:
            log(ERROR, f'No match found for attacks : {other}')

            raise Exception(f'No match found for attacks : {other}')

    match args['--aggregation']:
        case AggregationType.FED_AVG:
            log(INFO, "fed_avg")
            strategy = fl.server.strategy.FedAvg(evaluate_metrics_aggregation_fn=weighted_average)
        case AggregationType.KRUM:
            log(INFO, "krum")
            strategy = fl.server.strategy.Krum(evaluate_metrics_aggregation_fn=weighted_average)
        case 'fed_median':
            log(INFO, "fed_median")
            strategy = fl.server.strategy.FedMedian(evaluate_metrics_aggregation_fn=weighted_average)
        case other:
            log(ERROR, f'No match found for attacks : {other}')

            raise Exception(f'No match found for attacks : {other}')

    attack_server = AttackServer(
        strategy=strategy,
        attack=attack,
    )
    fl.server.start_server(
        server_address=f"0.0.0.0:{args['--port']}",
        config=fl.server.ServerConfig(num_rounds=args['--num_rounds']),
        server=attack_server
    )


if __name__ == "__main__":
    server()
