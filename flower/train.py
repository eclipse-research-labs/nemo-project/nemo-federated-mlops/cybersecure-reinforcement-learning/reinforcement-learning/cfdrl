# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


from experiment import init_experiment
from clientProcess import ClientProcess
import multiprocessing as mp
if __name__ == "__main__":
    mp.set_start_method('spawn')
    # Load model and data
    experiment = init_experiment()

    server_process = ClientProcess(experiment=experiment)
    server_process.start()

    numberClients = 0
    client_processes = [None for i in range(numberClients)]
    for i in range(numberClients):
        client_processes[i] = ClientProcess(experiment=experiment)
        client_processes[i].start()

    server_process.join()
    for i in range(numberClients):
        client_processes[i].join()