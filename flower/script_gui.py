# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


import datetime
from typing import Any

import customtkinter as ctk

from therutils.parser.criterion.federated import  AggregationType, TrainArgs,AttacksType


# TODO switch to pygame

def script_gui(
) -> TrainArgs:
    root = ctk.CTk()
    output: TrainArgs = TrainArgs()
    # place a label on the root window
    root.title('CFDRL')

    # frm = ctk.CTkFrame(root, padding=10)

    window_width: int = 800
    window_height: int = 300

    # get the screen dimension
    screen_width: int = root.winfo_screenwidth()
    screen_height: int = root.winfo_screenheight()

    # find the center point
    center_x: int = int(screen_width / 2 - window_width / 2)
    center_y: int = int(screen_height / 2 - window_height / 2)

    # set the position of the window to the center of the screen
    root.geometry(f'{window_width}x{window_height}+{center_x}+{center_y}')
    # root.iconbitmap('download.jpeg')

    message = ctk.CTkLabel(root, text="Which parameter to train?")
    message.grid(column=0, row=0)

    # exit button
    exit_button = ctk.CTkButton(
        root,
        text='Exit',
        command=lambda: root.quit()
    )

    message = ctk.CTkLabel(root, text="Train ")
    message.grid(column=0, row=2)

    message = ctk.CTkLabel(root, text="Number of clients ")
    message.grid(column=1, row=2)

    # Create the option menu widget and passing
    # the options_list and value_inside to it.
    clients_number_menu = ctk.CTkEntry(root)
    clients_number_menu.grid(column=2, row=2)
    clients_number_menu.insert(0, str(2))

    message_ezxp = ctk.CTkLabel(root, text="                   ")
    message_ezxp.grid(column=3, row=2)

    message_exp = ctk.CTkLabel(root, text="experiment name ")
    message_exp.grid(column=4, row=2)

    # Create the option menu widget and passing
    # the options_list and value_inside to it.
    exp_name_menu = ctk.CTkEntry(root)
    exp_name_menu.grid(column=5, row=2)
    exp_name_menu.insert(0, str(datetime.datetime.now()))

    message_des = ctk.CTkLabel(root, text="description ")
    message_des.grid(column=4, row=3)

    # Create the option menu widget and passing
    # the options_list and value_inside to it.
    des_name_menu = ctk.CTkEntry(root)
    des_name_menu.grid(column=5, row=3)
    des_name_menu.insert(0, 'description')

    message = ctk.CTkLabel(root, text=" Sharing Models ")
    message.grid(column=1, row=3, padx=10, pady=10)

    # Create the list of options
    sharing_model_options_list: list[str] = ["True", "False"]

    # Variable to keep track of the option
    # selected in OptionMenu
    sharing_model_choice = ctk.StringVar(value="True")  # set initial value

    # chipi_algo_choice = tk.StringVar(root)

    # Set the default value of the variable
    # chipi_algo_choice.set("RecurZipfBase3")

    # Create the option menu widget and passing
    # the options_list and value_inside to it.
    sharing_model_menu = ctk.CTkOptionMenu(
        master=root,
        values=sharing_model_options_list,
        variable=sharing_model_choice
    )
    sharing_model_menu.grid(column=2, row=3, padx=10, pady=10)

    message = ctk.CTkLabel(root, text="  Aggregation  ")

    message.grid(column=1, row=4, padx=10, pady=10)

    # Create the list of options
    aggregation_list = [e.value for e in AggregationType]

    # Variable to keep track of the option
    # selected in OptionMenu
    aggregation_value = ctk.StringVar(value=str(AggregationType.FED_AVG.value))  # set initial value


    # Create the option menu widget and passing
    # the options_list and value_inside to it.
    aggregation_menu = ctk.CTkOptionMenu(
        master=root,
        variable=aggregation_value,
        values=aggregation_list
    )
    aggregation_menu.grid(column=2, row=4)


    message = ctk.CTkLabel(root, text="  Attacks  ")

    message.grid(column=4, row=4, padx=10, pady=10)

    attack_list = [e.value for e in AttacksType]

    # Variable to keep track of the option
    # selected in OptionMenu
    attack_value = ctk.StringVar(value=str(AttacksType.NO_ATTACKS.value))  # set initial value


    # Create the option menu widget and passing
    # the options_list and value_inside to it.
    attack_menu = ctk.CTkOptionMenu(
        master=root,
        variable=attack_value,
        values=attack_list
    )
    attack_menu.grid(column=5, row=4)

    # play button
    train_button: ctk.CTkButton = ctk.CTkButton(
        root,
        text='!Train!',
        command=lambda: [
            train_args(
                output,
                number_clients=clients_number_menu,
                federated_learning=sharing_model_choice,
                aggregation=aggregation_value,
                description=des_name_menu,
                experiment=exp_name_menu,
                attacks=attack_value
            ),
            root.destroy()
        ]
    )

    train_button.grid(row=5, column=2, padx=10, pady=10)
    exit_button.grid(row=8, column=0, padx=10, pady=10)

    root.mainloop()
    gui_args: dict[str, Any]
    script_type: Any

    print(f'Gui choices: the args are {output}')
    return output


def train_args(
        output: TrainArgs,
        number_clients: ctk.CTkEntry,
        federated_learning: ctk.StringVar,
        aggregation: ctk.StringVar,
        description: ctk.CTkEntry,
        experiment: ctk.CTkEntry,
        attacks :ctk.StringVar
) -> bool:
    output.number_clients = int(number_clients.get())
    output.federated_learning = str(federated_learning.get())
    output.aggregation = str(aggregation.get())
    output.description = str(description.get())
    output.exp = str(experiment.get())
    output.attacks = str(attacks.get())

    return True


def watch_a_game(output: dict[str, Any]) -> bool:
    output['type'] = 'watch_a_game'
    return True


def visualize_a_tree(output: dict[str, Any]) -> bool:
    output['type'] = 'tree_visualization'
    return True
