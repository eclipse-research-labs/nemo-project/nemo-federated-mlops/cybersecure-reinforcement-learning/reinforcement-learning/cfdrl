# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


import multiprocessing
import flwr as fl
from client import RLClient


class ClientProcess(multiprocessing.Process):
    def __init__(self, experiment):
        # Call the Thread class's init function
        multiprocessing.Process.__init__(self, daemon=False)
        self._stop_event = multiprocessing.Event()
        self.experiment = experiment

    # Override the run() function of Thread class
    def run(self):
        print('Started Client Process : ')

        while not self.stopped():


            # Start Flower client
            fl.client.start_numpy_client(
                server_address="127.0.0.1:8080",
                client=RLClient(experiment=self.experiment),
            )

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()
