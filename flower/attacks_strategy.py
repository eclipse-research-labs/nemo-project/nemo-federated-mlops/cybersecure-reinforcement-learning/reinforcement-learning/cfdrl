# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #
#                                                                              #
# Copyright © 2021-2023 Thales SIX GTS FRANCE                                  #
#                                                                              #
# This release is part of the TheRLib-NEMO release and must be considered      #
# with respect to the NEMO_BACKGROUND document included as well as the         #
# NEMO agreements.                                                             #
# Please contact Thales SIX GTS FRANCE for more information on the license of  #
# the European project and the background declaration included.                #
#                                                                              #
# **************************************************************************** #
#                                   Copyright                                  #
# **************************************************************************** #


import flwr as fl
import numpy as np
from flwr.common import Parameters, Scalar, FitRes, Metrics, ReconnectIns, MetricsAggregationFn, NDArrays, \
    ndarrays_to_parameters, parameters_to_ndarrays
from typing import List, Tuple, Optional, Callable, Dict, Union
from flwr.server.client_proxy import ClientProxy
from flwr.server.strategy.aggregate import aggregate, weighted_loss_avg, aggregate_krum
from flwr.common.logger import log
from logging import WARNING  # we need those imports to implement the strategy class

from flwr.common.logger import log
from logging import INFO, DEBUG, ERROR


class AttackSimulationStrategy(
    fl.server.strategy.FedAvg):  # we inherit from FedAvg strategy and change only what we need
    def __init__(self, *, fraction_fit: float = 1.0, fraction_evaluate: float = 1.0, min_fit_clients: int = 2,
                 min_evaluate_clients: int = 2, min_available_clients: int = 2, num_malicious_clients: int = 2,
                 num_clients_to_keep: int = 0, evaluate_fn: Optional[Callable[[int, NDArrays, Dict[str, Scalar]],
            Optional[Tuple[
                float, Dict[str, Scalar]]],]] = None,
                 on_fit_config_fn: Optional[Callable[[int], Dict[str, Scalar]]] = None,
                 on_evaluate_config_fn: Optional[Callable[[int], Dict[str, Scalar]]] = None,
                 accept_failures: bool = True,
                 initial_parameters: Optional[Parameters] = None,
                 fit_metrics_aggregation_fn: Optional[MetricsAggregationFn] = None,
                 evaluate_metrics_aggregation_fn: Optional[MetricsAggregationFn] = None,
                 perturbationVector: str, adversaryKnowledge: str,
                 num_clients: int,
                 aggregation: str
                 ) -> None:
        super().__init__(
            fraction_fit=fraction_fit, fraction_evaluate=fraction_evaluate, min_fit_clients=min_fit_clients,
            min_evaluate_clients=min_evaluate_clients, min_available_clients=min_available_clients,
            evaluate_fn=evaluate_fn,
            on_fit_config_fn=on_fit_config_fn, on_evaluate_config_fn=on_evaluate_config_fn,
            accept_failures=accept_failures,
            initial_parameters=initial_parameters, fit_metrics_aggregation_fn=fit_metrics_aggregation_fn,
            evaluate_metrics_aggregation_fn=evaluate_metrics_aggregation_fn,
        )
        self.aggregation = aggregation
        self.histories = [None for _ in range(num_clients)]
        self.num_malicious_clients = num_malicious_clients
        self.num_clients_to_keep = num_clients_to_keep
        self.perturbationVector = perturbationVector
        self.adversaryKnowledge = adversaryKnowledge
        self.max_epsilon = 0.0  # this is the global privacy budget

    def __repr__(self) -> str:
        rep = f"AttackSimulationStrategy(accept_failures={self.accept_failures})"
        return rep

    def aggregate_fit(
            self,
            server_round: int,
            results: List[Tuple[ClientProxy, FitRes]],
            failures: List[Union[Tuple[ClientProxy, FitRes], BaseException]]
    ) -> Tuple[Optional[Parameters], Dict[str, Scalar]]:

        # convert results
        total_weights_results = [(parameters_to_ndarrays(fit_res.parameters), fit_res.num_examples) for _, fit_res in
                                 results]

        #############################################################################
        ######################### ----SIMULATE THE ATTACK---- #########################
        #############################################################################
        benign_weights_results = [(parameters_to_ndarrays(fit_res.parameters), fit_res.num_examples) for _, fit_res in
                                  results
                                  if fit_res.metrics["intention"] == "BENIGN"]
        if self.adversaryKnowledge == "agr-only" or self.adversaryKnowledge == "agnostic":
            malicious_weights_results = [(parameters_to_ndarrays(fit_res.parameters), fit_res.num_examples) for
                                         _, fit_res in results
                                         if fit_res.metrics["intention"] == "MALICIOUS"]
            best_parameters = aggregate(total_weights_results)  # malicious_weights_results
        else:
            best_parameters = aggregate(total_weights_results)

        # so far we have [1]: all clients parameters, [2]: benign clients parameters (seperate),
        #                [3]: malicious clients parameters (seperate), [4]: best_parameters (based on the adversary's knowledge)
        # no we have to optimize the model poisoning attack based on the adversary's knowledge and the perturbation vector

        # calculate the perturbation vector
        if self.perturbationVector == "InverseSign":
            perturbationVector = [-np.sign(best_parameter) for best_parameter in best_parameters]
        if self.perturbationVector == "InverseStd":
            parameters = [parameters for parameters, _ in total_weights_results]
            perturbationVector = []

            for parameterIdx in range(len(parameters[0])):
                arr = []
                for clientIdx in range(len(parameters)):
                    arr.append(parameters[clientIdx][parameterIdx])
                numpyArr = np.stack(arr)
                perturbationVector.append(-np.std(numpyArr, axis=0))

        # optimize for gamma
        gamma_init = 100.0
        gamma = gamma_init
        gamma_succ = 0.0
        t = 0.000001  # threshold
        step = gamma_init

        while True:  # this is a c++ do-while loop emulation
            print('oooo')
            malicious_parameters = []
            for idx in range(len(perturbationVector)):

                malicious_parameters.append(best_parameters[idx] + gamma * perturbationVector[idx])

            new_malicious_weights_results = [(malicious_parameters, num_examples) for _, num_examples in
                                             malicious_weights_results]
            new_total_weights_results = new_malicious_weights_results + benign_weights_results

            are_equal = True
            krum_parameters = aggregate_krum(new_total_weights_results, self.num_malicious_clients,
                                             self.num_clients_to_keep)
            for idx in range(len(krum_parameters)):
                if np.array_equal(malicious_parameters[idx], krum_parameters[idx]) == False:
                    are_equal = False
                    break

            if are_equal:
                gamma_succ = gamma
                gamma = gamma + step / 2
            else:
                gamma = gamma - step / 2
            step = step / 2

            if abs(gamma_succ - gamma) < t:
                break


        gamma_succ = 100

        print('ppp',gamma, gamma_succ, len([1 for _, fit_res in results if fit_res.metrics["intention"] == "MALICIOUS"]))

        malicious_parameters = []
        for idx in range(len(perturbationVector)):
            malicious_parameters.append(best_parameters[idx] + gamma_succ * perturbationVector[idx])
        new_malicious_weights_results = [(malicious_parameters, num_examples) for _, num_examples in
                                         malicious_weights_results]
        total_weights_results = new_malicious_weights_results + benign_weights_results
        #############################################################################
        #############################################################################
        #############################################################################

        match self.aggregation:
            case 'base':
                log(INFO, f"BAAAAAAAAAAAAAASE")

                # calculate Non Attack AGR
                parameters_aggregated = ndarrays_to_parameters(aggregate(total_weights_results))

            case 'krum':  # calculate Attack AGR
                log(INFO, f"KRUMMMMMMMMMMMMMMMMMM")

                parameters_aggregated = ndarrays_to_parameters(aggregate_krum(  # this thing here is for krum-AGR
                    total_weights_results, self.num_malicious_clients, self.num_clients_to_keep))
            case other:
                raise Exception(f'aggrgation? {other}')

        # boilerplate code if aggregation metrics are provided
        metrics_aggregated = {}
        if self.fit_metrics_aggregation_fn:
            fit_metrics = [(res.num_examples, res.metrics) for _, res in results]
            metrics_aggregated = self.fit_metrics_aggregation_fn(fit_metrics)
        elif server_round == 1:  # Only log this warning once
            log(WARNING, "No fit_metrics_aggregation_fn provided")

        return parameters_aggregated, metrics_aggregated
