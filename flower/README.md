# Launching the training in CFDRL
python train2.py --exp needit1e56_federated --number_clients 2 --exp_conf inputs/exp_optionsDQNacro.json --description federated156 --port 5987


python train2.py --exp nameofexp --number_clients 5 --exp_conf inputs/exp_optionsDQNacro.json --description description --federated_learning True --port 5833 --scenario attack --attack poison --aggregation krum

# Model sharing and model serving
During the training phase the models are automatically stored by the CFDRL in the bentoML repository.

A bento can be built from one of the learned model useing the 
```
bentoml build
```
This command will use the latest learned model as described in the bentofile.yaml

One can serve the model as 
```
bentoml serve MODEL_TAG
```

or export/import it as 
```
bentoml export MODEL_TAG .
```
```
bentoml import MODEL_TAG.bento
```